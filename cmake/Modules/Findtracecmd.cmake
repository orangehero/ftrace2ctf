# - Find trace-cmd
# This module accepts the following optional variables:
#    TRACECMD_PATH_HINT   = A hint on trace-cmd install path.
#
# This module defines the following variables:
#    tracecmd_FOUND       = Was Babeltrace found or not?
#    tracecmd_LIBRARIES   = The list of libraries to link to when using Babeltrace
#    tracecmd_INCLUDE_DIRS = The path to Babeltrace include directory
#
# On can set TRACECMD_PATH_HINT before using find_package(Babeltrace) and the
# module with use the PATH as a hint to find Babeltrace.
#
# The hint can be given on the command line too:
#   cmake -DTRACECMD_PATH_HINT=/home/chris/trace-cmd /path/to/source

if(TRACECMD_PATH_HINT)
  message(STATUS "Findtracecmd: using PATH HINT: ${TRACECMD_PATH_HINT}")
else()
  set(TRACECMD_PATH_HINT)
endif()

find_path(tracecmd_INCLUDE_DIR
          NAMES trace-cmd.h
          PATHS ${TRACECMD_PATH_HINT}
          DOC "The trace-cmd include headers")
		  
find_path(tracecmd_LIBRARY_DIR
          NAMES libtracecmd.a
          PATHS ${TRACECMD_PATH_HINT}
          DOC "The trace-cmd libraries")
		  		  
message(STATUS "Looking for trace-cmd build-dir ...")
if (${tracecmd_LIBRARY_DIR} MATCHES tracecmd_LIBRARY_DIR-NOTFOUND)
  set(tracecmd_FOUND FALSE)
  message(FATAL_ERROR "libtracecmd.a not found")
else()
   set(tracecmd_FOUND TRUE)
   set(tracecmd_INCLUDE_DIRS ${tracecmd_INCLUDE_DIR})
   set(tracecmd_LIBRARIES ${tracecmd_LIBRARY_DIR}/libparsevent.a ${tracecmd_LIBRARY_DIR}/libtracecmd.a)
   message(STATUS "libtracecmd.a found at ${tracecmd_LIBRARY_DIR}")
   message(STATUS "trace-cmd.h found at ${tracecmd_INCLUDE_DIR}")
endif()
