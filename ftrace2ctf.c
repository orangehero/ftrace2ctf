/* 
 * Copyright (C) 2016 Mansky Christian
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */

#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>

#include "trace-cmd.h"

#include <babeltrace/ctf-writer/writer.h>
#include <babeltrace/ctf-writer/clock.h>
#include <babeltrace/ctf-writer/stream.h>
#include <babeltrace/ctf-ir/event.h>
#include <babeltrace/ctf-ir/event-class.h>
#include <babeltrace/ctf-ir/fields.h>
#include <babeltrace/ctf-ir/field-types.h>

static struct bt_ctf_clock        * s_ctf_clock = 0;
static struct bt_ctf_writer       * s_ctf_writer = 0;
static struct bt_stream_class     * s_ctf_stream_class = 0;
static struct bt_stream           * s_ctf_stream = 0;
static enum   bt_ctf_byte_order     s_ctf_byte_order = BT_CTF_BYTE_ORDER_LITTLE_ENDIAN;

static unsigned int sDebugCnt = 0;

#define FTRACE_SYSCALL_ENTER_PREFIX 		"sys_enter_"
#define FTRACE_SYSCALL_EXIT_PREFIX 			"sys_exit_"
#define FTRACE_KMEM_PAGE                    "mm_page_"

/*****************************************************************************/
/*
 * OK, the method bt_ctf_stream_class_get_event_class_by_name was removed from
 * babeltrace stable/2.0 branch due to issues with event names not being required
 * to be unique; hence, the method should really return a list of event classes.
 * Since we know that our little application has unique event class names we don't
 * care about it. 
 *
 * Replaced with bt_stream_class_get_event_class_by_id.
 * TODO: remove this permanently from code.
 */
#if 0
struct bt_event_class * own_bt_ctf_stream_class_get_event_class_by_name(struct bt_stream_class * stream_class, const char * event_name, int eventid) {
	struct bt_event_class * ev_class = 0;
	int64_t cnt = bt_stream_class_get_event_class_count(stream_class);
	
	if (cnt > 0) {
		for (uint64_t i=0; i<cnt; i++) {
			ev_class = bt_stream_class_get_event_class_by_index(stream_class, (uint64_t)i);
			const char * name = bt_event_class_get_name(ev_class);
			if (strcmp(name, event_name) == 0) break;
			else ev_class = 0;
		}
	}
	
	struct bt_event_class * ev_class_2 = bt_stream_class_get_event_class_by_id(stream_class, (uint64_t)eventid);

	return ev_class;
}
#endif

/*****************************************************************************/
bool is_string (struct format_field *field, int len) {

	if (field->flags & FIELD_IS_STRING)
		return true;

	if ((field->flags & FIELD_IS_LONG) || (field->flags & FIELD_IS_POINTER)) {
		return false;
	}

	return (strstr(field->type, "char") != 0 && len > 0);
}

/*****************************************************************************/
bool is_integer(struct format_field *field, int len) {
	if ((field->flags & FIELD_IS_ARRAY) || (field->flags & FIELD_IS_DYNAMIC) || (field->flags & FIELD_IS_STRING)) {
		return false;
	}

	return (field->flags & FIELD_IS_POINTER) ||
			(field->flags & FIELD_IS_LONG) ||
			(len > 0 && len <=8);
}

/*****************************************************************************/
/*
 * The first field is alwys nr and holds the id of the event. omit this field,
 * since its of no interest to the user ...
 * todo: is_id_field is part of the for constract to get first and next fields.
 * abstract this in getfirst, getnext functions.
 */
bool is_id_field(struct event_format *event, struct format_field *field) {
	return field == event->format.fields && strcmp(field->name, "nr") == 0;
}

/*****************************************************************************/
void check_ctf_ret_val(int retval, unsigned int line, unsigned int counter) {
	if (retval != 0) {
		printf ("ERROR ctf: in line %u, counter = %u\n", line, counter);
		exit(-1);
	}
}

/*****************************************************************************/
/*
 * Function to ensure naming convention of Lttng. According to LttngEventLayout.java in
 * org.eclipse.tracecompass.internal.lttng2.kernel.core.trace.layout.
 */
const char * get_LttngEventLayout (struct event_format *event, struct format_field *field, char * name_buffer, size_t size_buffer) {
	char * substr = 0;
	if (strcmp(event->system, "syscalls")==0) {
		if (field == 0) {
			// sys call exit
			if (strstr(event->name, FTRACE_SYSCALL_EXIT_PREFIX) != 0) {
				strncpy(name_buffer, "syscall_exit_", size_buffer);
				strcat(name_buffer, event->name + strlen(FTRACE_SYSCALL_EXIT_PREFIX));
				return name_buffer;
			// sys call enter
			} else if (strstr(event->name, FTRACE_SYSCALL_ENTER_PREFIX) != 0) {
				strncpy(name_buffer, "syscall_entry_", size_buffer);
				strcat(name_buffer, event->name + strlen(FTRACE_SYSCALL_ENTER_PREFIX));
				return name_buffer;
			}
		}
	} else if (strcmp(event->system, "sched")==0) {
		if (field != 0) {
			// pid to tid
			substr = strstr(field->name, "pid");
			if (substr) {
				*substr = 't';
				return field->name;
			}
		}
	} else if (strcmp(event->system, "kmem")==0) {
		if (field == 0) {
			if (strstr(event->name, FTRACE_KMEM_PAGE) != 0) {
				strncpy(name_buffer, "kmem_", size_buffer);
				strcat(name_buffer, event->name);
				return  name_buffer;
			}
		}
	} else if (strcmp(event->system, "irq")==0) {
		if (field == 0) {
			if (strstr(event->name, "softirq_") != 0) {
				strncpy(name_buffer, "irq_", size_buffer);
				strcat(name_buffer, event->name);
				return name_buffer;
			}
		}
	} else if (strcmp(event->system, "timer")==0) {
		if (field == 0) {
			if (strstr(event->name, "hrtimer_") != 0) {
				strncpy(name_buffer, "timer_", size_buffer);
				strcat(name_buffer, event->name);
				return name_buffer;
			}
		}
	}

	/*
	 * no conversion required. get the original event or field name.
	 */
	if (field == 0) {
		return event->name;
	} else {
		return field->name;
	}

	return 0;
}

/*****************************************************************************/
/*
 * Defaults to 1ns/sec, except for tsc or counter
 */
uint64_t get_frequency(const char * trace_clock) {
	if (!strcmp(trace_clock, "local") || !strcmp(trace_clock, "global") ||
		!strcmp(trace_clock, "uptime") || !strcmp(trace_clock, "perf") ||
		!strcmp(trace_clock, "mono"))
		return 1000000000ULL;
	return 0;
}

/*****************************************************************************/
/*
 * Create
 */
void create_ctf_infrastructure(const char * outputdir, struct pevent * pevent, uint64_t abstime, char * uname) {
	uint64_t clock_freq = 0;
	s_ctf_writer = bt_ctf_writer_create(outputdir);
	struct bt_ctf_trace * ctf_trace = bt_ctf_writer_get_trace(s_ctf_writer);

	s_ctf_byte_order = pevent_is_file_bigendian(pevent) ? BT_CTF_BYTE_ORDER_BIG_ENDIAN : BT_CTF_BYTE_ORDER_LITTLE_ENDIAN;
	bt_ctf_writer_set_byte_order(s_ctf_writer, s_ctf_byte_order);

	/*
	 * environment uname
	 */
	char * saveptr = 0;
	char * tok = strtok_r (uname, " \n", &saveptr);
	if (tok) bt_trace_set_environment_field_string(ctf_trace, "host", tok);
	else bt_trace_set_environment_field_string(ctf_trace, "host", "unknown");

	tok = strtok_r (0, " \n", &saveptr);
	if (tok) bt_trace_set_environment_field_string(ctf_trace, "sysname", tok);
	else bt_trace_set_environment_field_string(ctf_trace, "sysname", "Linux"); // thats a safe guess?!

	tok = strtok_r (0, " \n", &saveptr);
	if (tok) bt_trace_set_environment_field_string(ctf_trace, "release", tok);
	else bt_trace_set_environment_field_string(ctf_trace, "release", "unknown");

	tok = strtok_r (0, " \n", &saveptr);
	if (tok) bt_trace_set_environment_field_string(ctf_trace, "machine", tok);
	else bt_trace_set_environment_field_string(ctf_trace, "machine", "unknown");

	/*
	 * environment lttng
	 */
	bt_trace_set_environment_field_string(ctf_trace, "domain", "kernel");
	bt_trace_set_environment_field_string(ctf_trace, "tracer_name", "lttng-modules");
	bt_trace_set_environment_field_integer(ctf_trace, "tracer_major", 2);
	bt_trace_set_environment_field_integer(ctf_trace, "tracer_minor", 8);

	/*
	 * clock
	 */
	s_ctf_clock = bt_ctf_clock_create(pevent->trace_clock);
	bt_ctf_clock_set_description(s_ctf_clock, "ftrace clock");
	if ((clock_freq = get_frequency(pevent->trace_clock))) bt_ctf_clock_set_frequency(s_ctf_clock, clock_freq);
	if (abstime) {
		bt_ctf_clock_set_offset(s_ctf_clock, abstime);
	}
	bt_ctf_clock_set_is_absolute(s_ctf_clock, 0);
	bt_ctf_writer_add_clock(s_ctf_writer, s_ctf_clock);

	/*
	 * stream_class
	 */
	s_ctf_stream_class = bt_stream_class_create("ftrace_stream");
	bt_stream_class_set_clock(s_ctf_stream_class, s_ctf_clock);
	/* add cpuid to packet context */
	struct bt_field_type *context = bt_stream_class_get_packet_context_type(s_ctf_stream_class);
	struct bt_field_type *cpuid_type = bt_field_type_integer_create(32);
	bt_field_type_integer_set_is_signed(cpuid_type, 0);
	bt_field_type_integer_set_base(cpuid_type, BT_CTF_INTEGER_BASE_DECIMAL);
	bt_field_type_set_byte_order(cpuid_type, s_ctf_byte_order);
	bt_field_type_set_alignment(cpuid_type, 8);
	bt_field_type_structure_add_field(context, cpuid_type, "cpu_id");
}

/*****************************************************************************/
/*
 * Translate a print event into ctf
 */
struct bt_event_class * create_ctf_event_class_ftrace(const char * event_name, const char * fieldname, struct event_format *event, int record_size) {
	struct bt_event_class * ctf_event_class = 0;

	/*
	 * Omit the function which printed the string and simply print the string.
	 */
	ctf_event_class = bt_event_class_create(event_name);

	struct bt_field_type *ctf_field_type = bt_field_type_string_create();
	bt_field_type_string_set_encoding(ctf_field_type, BT_CTF_STRING_ENCODING_UTF8);
	bt_field_type_set_alignment(ctf_field_type, 1);

	bt_event_class_set_id(ctf_event_class, event->id);

	bt_event_class_add_field(ctf_event_class, ctf_field_type, fieldname);

	bt_stream_class_add_event_class(s_ctf_stream_class, ctf_event_class);


	return ctf_event_class;
}

/*****************************************************************************/
/*
 * Translate a generic event field by field into ctf
 */
struct bt_event_class * create_ctf_event_class_generic(const char * event_name, struct event_format *event, int record_size) {
	struct format_field *field = 0;
	struct bt_event_class * ctf_event_class = bt_event_class_create(event_name);
	/*
	 * create type fields
	 */
	for (field = event->format.fields; field != 0; field = field->next) {
		if (is_id_field(event, field)) continue;

		/*
		 * Zero sized fields, mean the rest of the data
		 */
		int len = field->size ? : record_size - field->offset;
		struct bt_field_type *ctf_field_type = 0;
		if (is_string(field, len)) {
			ctf_field_type = bt_field_type_string_create();
			bt_field_type_string_set_encoding(ctf_field_type, BT_CTF_STRING_ENCODING_UTF8);
			bt_field_type_set_alignment(ctf_field_type, 1);
		} else if (is_integer(field, len)) {
			ctf_field_type = bt_field_type_integer_create(len * 8); // byte to bit!
			bt_field_type_set_alignment(ctf_field_type, 1);
			bt_field_type_integer_set_is_signed(ctf_field_type, field->flags & FIELD_IS_SIGNED);
			bt_field_type_integer_set_base(ctf_field_type,
					(field->flags & FIELD_IS_POINTER) ? BT_CTF_INTEGER_BASE_HEXADECIMAL : BT_CTF_INTEGER_BASE_DECIMAL);
			bt_field_type_integer_set_encoding(ctf_field_type, BT_CTF_STRING_ENCODING_NONE);
			bt_field_type_set_alignment(ctf_field_type, 8);
			bt_field_type_set_byte_order(ctf_field_type, s_ctf_byte_order);
		}
		else {
			continue;
		}
		bt_event_class_set_id(ctf_event_class, event->id);
		/*
		 * this is a lttng/tracecompass "specialty". pid in the sched events must be tid.
		 */
		char field_name_buffer [64] = {0};
		const char * field_name = get_LttngEventLayout(event, field, field_name_buffer, sizeof(field_name_buffer));

		bt_event_class_add_field(ctf_event_class, ctf_field_type, field_name);
	}

	bt_stream_class_add_event_class(s_ctf_stream_class, ctf_event_class);

	return ctf_event_class;
}

/*****************************************************************************/
/*
 * returns a ctf event class corresponding to ftrace event.
 */
struct bt_event_class * get_ctf_event_class(struct event_format *event, int record_size) {

	/*
	 * lttng provides only one version of sys_exit.
	 * todo: check if all sys_exit events have the same fields!
	 */
	char event_name_buffer [64] = {0};
	const char * event_name = get_LttngEventLayout(event, 0, event_name_buffer, sizeof(event_name_buffer));

	struct bt_event_class * ctf_event_class = bt_stream_class_get_event_class_by_id(s_ctf_stream_class, (uint64_t)event->id);
	/*
	 * event_class doesn't exist yet.
	 */
	if (ctf_event_class == 0) {
		/*
		 * create_ctf_event_class_print
		 * create_ctf_event_class_printk
		 * create_ctf_event_class_functions
		 * ...
		 */
		if (strcmp(event->system, "ftrace") == 0 && strcmp(event->name, "print") == 0) {
			ctf_event_class = create_ctf_event_class_ftrace(event_name, "usertrace", event, record_size);
		} else if (strcmp(event->system, "ftrace") == 0 && strcmp(event->name, "function") == 0) {
			ctf_event_class = create_ctf_event_class_ftrace(event_name, "f", event, record_size);
		} else if (strcmp(event->system, "ftrace") == 0 && (strcmp(event->name, "funcgraph_entry") == 0 || strcmp(event->name, "funcgraph_exit") == 0)) {
			ctf_event_class = create_ctf_event_class_ftrace(event_name, "fg", event, record_size);
		} else {
			ctf_event_class = create_ctf_event_class_generic(event_name, event, record_size);
		}
	}

	return ctf_event_class;
}

/*****************************************************************************/
/*
 * function creates and fills the field of print.
 */
void ctf_fields_create_and_fill_ftrace(struct pevent_record *record, struct event_format *event,
		                                struct bt_event_class * event_class, struct bt_event * ctf_event, const char * field_name) {

	int r = -1;

	struct trace_seq s;
	trace_seq_init(&s);
	pevent_event_info(&s, event, record);
	trace_seq_terminate(&s);

	struct bt_field *ctf_value = bt_field_create(bt_event_class_get_payload_type_field_type_by_name(event_class, field_name));
	r = bt_field_string_set_value(ctf_value, s.buffer);
	check_ctf_ret_val(r, __LINE__, sDebugCnt);
	r = bt_event_set_payload(ctf_event, field_name, ctf_value);
	check_ctf_ret_val(r, __LINE__, sDebugCnt);

}

/*****************************************************************************/
/*
 * function creates and fills ctf fields according to ftrace fields and their values.
 */
void ctf_fields_create_and_fill_generic(struct pevent_record *record, struct event_format *event,
		                                struct bt_event_class * event_class, struct bt_event * ctf_event) {
	int idx = 0, r = -1;
	struct format_field *field = 0;

	for (field = event->format.fields; field != 0; field = field->next, idx++) {
		if (is_id_field(event, field)) continue;

		unsigned int line = 0;
		struct bt_field *ctf_value = 0;
		char field_name_buffer [64] = {0};
		const char * field_name = get_LttngEventLayout(event, field, field_name_buffer, sizeof(field_name_buffer));

		/*
		 * Zero sized fields, mean the rest of the data
		 */
        int len = field->size ? : record->size - field->offset;

		if (is_string(field, len)) {
			line = __LINE__;

			struct trace_seq p;
			trace_seq_init(&p);
			pevent_print_field(&p, record->data, field);
			trace_seq_terminate(&p);

			ctf_value = bt_field_create(bt_event_class_get_payload_type_field_type_by_name(event_class, field_name));
			r = bt_field_string_set_value(ctf_value, p.buffer);

		} else if (is_integer(field, len)) {
			unsigned long long val = -1;
			pevent_read_number_field(field, record->data, &val);

			ctf_value = bt_field_create(bt_event_class_get_payload_type_field_type_by_name(event_class, field_name));
			if (field->flags & FIELD_IS_SIGNED) {
				long long llval = val;
				if (field->size == 1) {
					llval = (int8_t)val;
				} else if (field->size == 2) {
					llval = (int16_t)val;
				} else if (field->size == 4) {
					llval = (int32_t)val;
				}
				r = bt_field_signed_integer_set_value(ctf_value, llval);
				line = __LINE__;
			} else {
				r = bt_field_unsigned_integer_set_value(ctf_value, val);
				line = __LINE__;
			}

		} else {
			/* not a supported type ... */
			continue;
		}
		check_ctf_ret_val(r, line, sDebugCnt);

		r = bt_event_set_payload(ctf_event, field_name, ctf_value);
		check_ctf_ret_val(r, __LINE__, sDebugCnt);

		sDebugCnt++;
	}
}

/*****************************************************************************/
int pevent_print_event_2ctf (struct pevent_record *record, struct event_format *event) {

	int r = -1;

	if (event == 0) return 0;

	struct bt_event_class * event_class = get_ctf_event_class(event, record->size);
	struct bt_event * ctf_event = bt_event_create(event_class);
	int64_t event_id = bt_event_class_get_id(event_class);

	/*
	 * now correct the event header
	 */
	struct bt_field *ctf_event_header = bt_event_get_header(ctf_event);
	// set timestamp, set id
	/*struct bt_field * ctf_ts = bt_field_structure_get_field(ctf_event_header, "timestamp");
	int ret = bt_field_unsigned_integer_set_value(ctf_ts, record->ts);*/
	bt_ctf_clock_set_time(s_ctf_clock, (int64_t)record->ts);

	struct bt_field * ctf_id = bt_field_structure_get_field_by_name(ctf_event_header, "id");
	bt_field_unsigned_integer_set_value(ctf_id, event_id);

	/*
	 * create fields and fill them with values.
	 */
	if (strcmp(event->system, "ftrace") == 0 && strcmp(event->name, "print") == 0) {
		ctf_fields_create_and_fill_ftrace(record, event, event_class, ctf_event, "usertrace");
	} else if (strcmp(event->system, "ftrace") == 0 && strcmp(event->name, "function") == 0) {
		ctf_fields_create_and_fill_ftrace(record, event, event_class, ctf_event, "f");
	} else if (strcmp(event->system, "ftrace") == 0 && (strcmp(event->name, "funcgraph_entry") == 0 || strcmp(event->name, "funcgraph_exit") == 0)) {
		ctf_fields_create_and_fill_ftrace(record, event, event_class, ctf_event, "fg");
	} else {
		ctf_fields_create_and_fill_generic(record, event, event_class, ctf_event);
	}

	r = bt_ctf_stream_append_event(s_ctf_stream, ctf_event);
	check_ctf_ret_val(r, __LINE__, sDebugCnt);

	return 0;
}

/*****************************************************************************/
/*
 * trace-cmd does not provide an api for that, but a function that prints uname
 * to stdout. Hence, fork, let the child print uname to a pipe and let the
 * parent read that uname from this pipe.
 */
void fetch_uname_from_traceinput_handle (struct tracecmd_input *handle, char * uname, int unamelen) {
	int pipefd [2] = {0};

	if (pipe(pipefd)==-1) {
		perror("Unable to obtain uname from tracecmd input handle. pipe create.\n");
		return;
	}

	int resfork = fork();
	if (resfork == -1) {
		perror ("Unable to obtain uname from tracecmd input handle. forking\n");
		return;
	}

	/*
	 * child printig uname to stdout, redirected to a pipe.
	 */
	if (resfork == 0) {
		dup2(pipefd[1], 1); // redirect stdout
		//sleep(1);
		tracecmd_print_uname(handle);
		fflush(stdout);
		close(pipefd[0]);
		close(pipefd[1]);
		_exit(EXIT_SUCCESS);

	/*
	 * parent reading uname from pipe.
	 */
	} else {
		close(pipefd[1]); // parent doesn't write
		read(pipefd[0], uname, unamelen);
		close(pipefd[0]);
	}
}

/*****************************************************************************/
int main(const int argc, const char * argv []) {

	if (argc != 3) {
		printf ("usage:\n");
		printf ("%s <trace.dat.filename> <outputdir>\n", argv[0]);
		return -1;
	}

	const char * filename = argv[1];
	const char * outputdir = argv[2];
	uint64_t abstime = 0;
	int r = -1, eventNr = 0, i = 0;

	struct tracecmd_input *handle = tracecmd_open(filename);
	if (handle == 0) {
		printf ("Could not find %s.", filename ? : "<nil>");
		return -2;
	}

	struct pevent * pevent = tracecmd_get_pevent(handle);
	int cpus = tracecmd_cpus(handle);

	/*
	 * read the uname from a pipe, theres no api for that ...
	 */
	char uname [64] = {0};
	fetch_uname_from_traceinput_handle(handle, uname, sizeof(uname));

	create_ctf_infrastructure(outputdir, pevent, abstime, uname);

	for (i=0; i<cpus; i++) {

		/*
		 * create stream instance
		 */
		s_ctf_stream = bt_ctf_writer_create_stream(s_ctf_writer, s_ctf_stream_class);

		struct pevent_record * evrec = 0;
		evrec = tracecmd_read_cpu_first(handle, i);
		while (evrec) {
			struct event_format * event = pevent_find_event_by_record(pevent, evrec);
			if (!event) {
				printf("OOPS, unknown event ... continue!");
				continue;
			}
			pevent_print_event_2ctf(evrec, event);

			free_record(evrec);
			evrec = tracecmd_read_data(handle, i);

			eventNr++;
		}

		/*
		 * one stream per cpu
		 */
		struct bt_field *ctf_cpu_id = bt_field_structure_get_field_by_name(bt_ctf_stream_get_packet_context(s_ctf_stream), "cpu_id");
		r = bt_field_unsigned_integer_set_value(ctf_cpu_id, (int64_t) i);
		check_ctf_ret_val(r, __LINE__, i);

		r = bt_ctf_stream_flush(s_ctf_stream);
		check_ctf_ret_val(r, __LINE__, i);
	}

	/*
	 * now flush metadata
	 */
	bt_ctf_writer_flush_metadata(s_ctf_writer);
	bt_ctf_writer_put(s_ctf_writer);

	tracecmd_close(handle);

	return 0;
}
