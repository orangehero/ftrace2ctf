This is a small tool that converts a trace.dat file as produced by trace-cmd 
into ctf (lttng) format. It can be displayed with tracecompass, an eclipse 
plugin to visualize lttng kernel traces. 
tracecompass can be downloaded from 
https://projects.eclipse.org/projects/tools.tracecompass

ftrace2ctf depends on two static libraries provided by trace-cmd itself. These 
are linked into the trace-cmd binary and to my knowledge are not part of
any official package release. So you have to download and build trace-cmd for 
yourself.

A simple "git clone git://git.kernel.org/pub/scm/linux/kernel/git/rostedt/trace-cmd.git"
followed by make should be sufficient.

The second dependency is babeltrace (version >=2). At the time of this writing
there was no official release of babeltrace2, so you will have to make do 
with a pre release. Follow compile and installation instructions in the README
file.

The git repository can be downloaded with "git clone git://git.efficios.com/babeltrace.git"

In order to build type
cmake -DTRACECMD_PATH_HINT=/path/to/trac-cmd/src
make

For an explicit debug build create a directory parallel to your ftrace2ctf dir, and 
change into it. Then type:
cmake ../ftrace2ctf/ -DCMAKE_BUILD_TYPE=debug -DTRACECMD_PATH_HINT=/path/to/trace-cmd/src 

If everything goes well this results in a binary ftrace2ctf. Called without 
parameters it prints a brief help.
